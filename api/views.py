from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import *
from .serializer import *


class BacketleView(APIView):
    def get(self, request, **kwargs):
        pk = kwargs.get("pk", None)
        print(pk)
        if pk:
            backet = Backet.objects.get(id=pk)
            serializer = BacketSerializer(backet)
        else:
            backet = Backet.objects.all()
            serializer = BacketSerializer(backet, many=True)
        return Response({"backet": serializer.data})

    def post(self, request):
        backet = request.data
        serializer = BacketSerializer(data=backet)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response({"success": serializer.data})

    def put(self, request, pk):
        saved_article = get_object_or_404(Backet.objects.all(), pk=pk)
        data = request.data
        serializer = BacketSerializer(instance=saved_article, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response({
            "success": serializer.data
        })

    def delete(self, request, pk):
        article = get_object_or_404(Backet.objects.all(), pk=pk)
        article.delete()
        return Response({
            "message": F"Article with id {pk} has been deleted."
        }, status=204)
