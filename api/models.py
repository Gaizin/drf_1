from django.db import models


class Item(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()
    prise = models.CharField(max_length=10)


class Backet(models.Model):
    title = models.CharField(max_length=200)
    items = models.ManyToManyField(Item)
    full_prise = models.CharField(max_length=200)
