# Generated by Django 4.1.4 on 2023-03-02 11:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_company_remove_men_content_remove_men_is_published_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Men',
            new_name='Item',
        ),
        migrations.AlterModelOptions(
            name='item',
            options={'verbose_name': 'Item', 'verbose_name_plural': 'Items'},
        ),
    ]
