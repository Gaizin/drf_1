# Generated by Django 4.1.4 on 2023-03-02 11:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_rename_cat_men_cat_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=100)),
                ('country', models.CharField(db_index=True, max_length=100)),
            ],
            options={
                'verbose_name': 'Company',
                'verbose_name_plural': 'Companyes',
            },
        ),
        migrations.RemoveField(
            model_name='men',
            name='content',
        ),
        migrations.RemoveField(
            model_name='men',
            name='is_published',
        ),
        migrations.RemoveField(
            model_name='men',
            name='time_create',
        ),
        migrations.AddField(
            model_name='men',
            name='prise',
            field=models.CharField(default=22, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='men',
            name='size',
            field=models.CharField(default=100, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='men',
            name='company_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='api.company'),
        ),
    ]
